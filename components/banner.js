import React from "react";
import classes from "../styles/banner.module.css";
const Banner = (props) => {
  return (
    <div className={classes.container}>
      <h1 className={classes.title}>
        <span className={classes.title1}>Coffee</span>
        <span className={classes.title2}>Connoisseur</span>
      </h1>
      <p className={classes.subTitle}>Discover your local coffee shops</p>
      <button className={classes.button} onClick={props.handleClick}>
        {props.buttonText}
      </button>
    </div>
  );
};

export default Banner;
